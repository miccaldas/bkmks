""" Module to insert bookmarks to database """

import sqlite3
import fire
from colr import color


def add():
    titulo = input(color(' Title? » ', fore='#f29b85'))
    comentario = input(color(' Comment » ', fore='#f29b85'))
    link = input(color(' Link » ', fore='#f29b85'))
    kwd1 = input(color(' Choose a keyword » ', fore='#f29b85'))
    kwd2 = input(color(' Choose another ... » ', fore='#f29b85'))
    kwd3 = input(color(' And another... » ', fore='#f29b85'))

    answers = [titulo, comentario, link, kwd1, kwd2, kwd3]

    try:
        conn = sqlite3.connect('bkmk.db')
        cur = conn.cursor()
        query = """ INSERT INTO bkmk (title, comment, link, k1, k2, k3) VALUES (?, ?, ?, ?, ?, ?) """
        cur.execute(query, answers)
        conn.commit()

    except sqlite3.Error as e:
        print("Error while connecting to db", e)
    finally:
        if(conn):
            conn.close()


if __name__ == '__main__':
    fire.Fire(add)
