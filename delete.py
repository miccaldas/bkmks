""" Module to delete bookmarks from the database """

import sqlite3
from colr import color


def delete():
    ident = input(color(' ID to delete? » ', fore='#f29b85'))

    try:
        conn = sqlite3.connect('bkmk.db')
        cur = conn.cursor()
        query = " DELETE FROM bkmk WHERE bkid = " + ident
        cur.execute(query)
        conn.commit()

    except sqlite3.Error as e:
        print("Error while connecting to db", e)
    finally:
        if(conn):
            conn.close()


if __name__ == '__main__':
    delete()
