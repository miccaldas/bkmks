""" Module to search the database """

import sqlite3
from colr import color
import fire


def search():
    try:
        busca = input(color(' What are you searching for? ', fore='#f29b85'))
        conn = sqlite3.connect('bkmk.db')
        cur = conn.cursor()
        query = " SELECT * FROM bkmk_fts WHERE bkmk_fts MATCH '" + busca + "' ORDER BY rank"
        cur.execute(query)
        records = cur.fetchall()
        for row in records:
            print(color(' [*] ID » ', fore='#85a585'), color(str(row[0]), fore='#fdf3d3'))
            print(color(' [*] TITLE » ', fore='#85a585'), color(str(row[1]), fore='#fdf3d3'))
            print(color(' [*] COMMENT » ', fore='#85a585'), color(str(row[2]), fore='#fdf3d3'))
            print(color(' [*] LINK ? ', fore='#85a585'), color(str(row[3]), fore='#f29b85'))
            print(color(' [*] KEYWORD 1 » ', fore='#85a585'), color(str(row[4]), fore='#fdf3d3'))
            print(color(' [*] KEYWORD 2 » ', fore='#85a585'), color(str(row[5]), fore='#fdf3d3'))
            print(color(' [*] KEYWORD 3 » ', fore='#85a585'), color(str(row[6]), fore='#fdf3d3'))
            print(color(' [*] TIME » ', fore='#85a585'), color(str(row[7]), fore='#fdf3d3'))
            print('\n')
    except sqlite3.Error as e:
        print("Error while connecting to db", e)
    finally:
        if(conn):
            conn.close()


if __name__ == '__main__':
    fire.Fire(search)
