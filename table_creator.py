""" This module creates a SQLite database and defines and uploads a new table for said database"""
import collections
import sqlite3
from colr import color
from icecream import ic
import re

db_name = input(color('What name do you want for the database? ', fore='#585a47'))


def create_db():
    """Where we use the database name to create one."""

    try:
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()
        ic("Database created and Successfully Connected to SQLite")
        sqlite_select_Query = "select sqlite_version();"
        cur.execute(sqlite_select_Query)
        record = cur.fetchall()
        ic("SQLite Database Version is: ", record)
        conn.commit()
    except sqlite3.Error as e:
        ic("Error while connecting to db", e)
    finally:
        if(conn):
            conn.close()


if __name__ == '__main__':
    create_db()


def collect_data():
    """ Here we ask the user the information needed to create a table, then we process the output until is fit to be a SQL query, and uploaded it to the db"""

    a = collections.OrderedDict()
    table_name = input(color('What name do you want for the table? ', fore='#585a47'))
    col_num = int(input(color('How many columns will you need? ', fore='#ff6f69')))
    for y in range(col_num):
        col_name = input(color('What is the name of your column? ', fore='#ff6f69'))
        key = col_name
        a.setdefault(key, [])
        att_num = int(input(color('How many attributes will you need? ', fore='#ff6f69')))
        for x in range(att_num):
            att_name = input(color('What is the name of your attribute? ', fore='#ff6f69'))
            a[key].append(att_name)

    #  Creating an empty list,
    new = []
    #  Opening a loop through the list that houses the answers to the questions of the last code block
    for i in a:
        # Separating the column name and its associates attributes into a specific list
        b = (i, a[i])
        # Aggregating the information separated by columns and respective attributes into a list of lists
        c = list(b)
        # Appending the columns name to a new list, as a string. The transformation to string is to prepare for manipulation
        new.append(str(c[0]))
        # Isolating the attributes part of the list as string
        d = str(c[1])
        # Replacing the comma between the attributes by a space
        e = d.replace(',', '')
        # Aggregating the commaless attributes to the ‘new’ list
        new.append(e)
    # Transforming the list where you kept the column names and their attributes, after manipulation, into a string
    corda = str(new)
    # create a new string but without the ‘[]""'}’ characters in it.
    cor1 = corda.translate({ord(i): None for i in '[]""'})
    # Another pass, to remove the apostrophes
    cor2 = cor1.translate({ord(i): None for i in "'"})
    # First we isolate the comma character in a variable.
    sub = ','
    # look for matches of commas in our clean string
    matches = re.finditer(sub, cor2)
    # We identify their indices, and keep them in a list
    matches_positions = [match.start() for match in matches]
    oc = matches_positions
    #  it’s necessary to delete commas, skipping one value and starting from 0
    oc1 = oc[0::2]
    # turn the string with the clean query into a list, so as to iterate through it
    cor3 = list(cor2)
    #  open a iterator, setting as condition the indices on the list of the ones to replace
    for idx in oc1:
        # Replacing the indices on the list of the query, with spaces, on the indexes that were marked in oc1
        cor3[idx] = ''
    # Since we turned a string into a list, it separated all characters into different items. It’s needed to join them back again.
    cor4 = ''.join(cor3)
    # Finally we define the structure of the query.
    query = 'CREATE TABLE ' + table_name + '(' + cor4 + ')'

    try:
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()
        cur.execute(query)
        conn.commit()

    except sqlite3.Error as e:
        print("Error while connecting to db", e)
    finally:
        if(conn):
            conn.close()


if __name__ == '__main__':
    collect_data()
