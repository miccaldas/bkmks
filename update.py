""" Module to update bookmarks to database """
import time
import sqlite3
import click
from colr import color


def update():
    coluna = input(color(' Column? » ', fore='#f29b85'))
    ident = input(color(' ID? » ', fore='#f29b85'))
    print(color(' Write your update', fore='#f29b85'))
    time.sleep(0.3)
    update = click.edit()

    try:
        conn = sqlite3.connect('bkmk.db')
        cur = conn.cursor()
        query = "UPDATE bkmk SET " + coluna + " = '" + update + "' WHERE bkid = " + ident
        cur.execute(query,)
        conn.commit()

    except sqlite3.Error as e:
        print("Error while connecting to db", e)
    finally:
        if(conn):
            conn.close()


if __name__ == '__main__':
    update()
